<?php get_header(); ?>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php bloginfo('wpurl');?>">Phoenix</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <!-- <li class="active"><a href="#">Home</a></li> -->
            <?php wp_list_pages( '&title_li='); ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1><?php echo get_bloginfo( 'name' ); ?></h1>
        <h4><?php echo get_bloginfo( 'description' ); ?></h4>
      </div>

      <div class="col-md-9">
        <p class="lead">
            <?php
        			if ( have_posts() ) : while ( have_posts() ) : the_post();
        				get_template_part( 'content', get_post_format() );
        			endwhile; endif;
      			?>
        </p>
      </div>
      <?php get_sidebar(); ?>

<?php get_footer(); ?>
